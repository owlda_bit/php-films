<?php
$link = mysqli_connect("localhost", "root", "", "project-db");

if($link === false){
    die("ERROR: Could not connect. " . mysqli_connect_error());
}

$sql = "SELECT * FROM movies";
if($result = mysqli_query($link, $sql)){
    if(mysqli_num_rows($result) > 0){

        print "<table class='table-films'>";
        while($row = mysqli_fetch_array($result)){
            print"<tr><form method='post'>";
            print "<td><input name='idFilm' value='".$row['id']."'></td>";
            print "<td><input name='titleFilm' value='".$row['title']."'></td>";
            print "<td><input name='authorFilm' value='".$row['author']."'></td>";
            print "<td><input name='priceFilm' value='".$row['price']."'></td>";
            print "<td><input name='categoryFilm' value='".$row['category']."'></td>";
            print "<td><input name='urlFilm' value='".$row['url']."'></td>";
            print "<td><button type='submit' name='ModifierFilm' class=\"btn btn-info\" role=\"button\" style='margin-left: 20px'>Modifier</button></td>";
            print "<td><button type='submit' name='SuprimerFilm' class=\"btn btn-danger\" role=\"button\" style='margin-left: 20px'>Suprimer</button></form></td>";
            print"</tr>";        }
        echo "</table>";
        mysqli_free_result($result);
      } else{
        echo "No records matching your query were found.";
    }
} else{
    echo "ERROR: Could not able to execute $sql. " . mysqli_error($link);
}
mysqli_close($link);


if (isset($_POST['ModifierFilm'])) {

    require APPROOT . '/models/Movie.php';

    try {
        $user = 'root';
        $password = '';
        $server ='localhost';

        // Prepare the PDO
        $conn = new PDO("mysql:host=$server;dbname=project-db", $user, $password);
        $conn -> setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);

        // Pre-statement for query
        $stmt= $conn->prepare("UPDATE movies SET title=:title, author=:author, url=:url, price=:price, category=:category WHERE id=:id");

        $stmt->execute(array(':id' => (int) $_POST['idFilm'], ':title' => $_POST['titleFilm'], ':author' => $_POST['authorFilm'], ':url' => $_POST['urlFilm'], ':price' => $_POST['priceFilm'], ':category' => $_POST['categoryFilm'] ));

    } catch (PDOException $e) {
        print 'Failed to update: ' . $e->getMessage();
    }


}

if (isset($_POST['SuprimerFilm'])) {

    require APPROOT . '/models/Movie.php';

    try {
        $user = 'root';
        $password = '';
        $server = 'localhost';

        // Prepare the PDO
        $conn = new PDO("mysql:host=$server;dbname=project-db", $user, $password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $stmt= $conn->prepare("DELETE FROM movies WHERE id=:id");
        $stmt->execute(array(':id' => (int) $_POST['idFilm'] ));

    }
    catch (PDOException $e) {
    print 'Failed to delete: ' . $e->getMessage();
    }
}



